using System;
using System.Collections.Generic;
using System.Text;

namespace hackathon.Models.ResponseModels.Case
{
    public class CaseListDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string SubjectName { get; set; }
        public string CaseRefNo { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
