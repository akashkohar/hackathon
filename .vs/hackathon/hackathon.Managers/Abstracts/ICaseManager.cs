using hackathon.Models.RequestModels.Case;
using hackathon.Models.ResponseModels.Case;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace hackathon.Managers.Abstracts
{
    public interface ICaseManager
    {
        Task AddAsync(AddCaseVm model);
        Task<IList<CaseListDto>> GetAsync();
    }
}
