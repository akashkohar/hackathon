using hackathon.DataLayer.Abstracts;
using hackathon.DataLayer.Entities;
using hackathon.Managers.Abstracts;
using hackathon.Models.RequestModels.Case;
using hackathon.Models.ResponseModels.Case;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace hackathon.Managers
{
    public class CaseManager : ICaseManager
    {
        private readonly IConfiguration _configuration;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Case> _repository;

        public CaseManager(IConfiguration configuration,
            IUnitOfWork unitOfWork,
            IRepository<Case> repository)
        {
            _configuration = configuration;
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        public async Task AddAsync(AddCaseVm model)
        {
            var data = new Case
            {
                CaseRefNo = model.CaseRefNo,
                Description = model.Description,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                SubjectName = model.SubjectName,
                Title = model.Title,
                CreatedDate = DateTime.Now,
                ModifiedDate = DateTime.Now
            };

            await _repository.InsertAsync(data);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<IList<CaseListDto>> GetAsync()
        {
            return await _repository.Entity()
                                    .Select(x => new CaseListDto
                                    {
                                        CaseRefNo = x.CaseRefNo,
                                        StartDate = x.StartDate,
                                        SubjectName = x.SubjectName,
                                        Description = x.Description,
                                        EndDate = x.EndDate,
                                        Id = x.Id,
                                        Title = x.Title
                                    }).ToListAsync();
        }
    }
}
