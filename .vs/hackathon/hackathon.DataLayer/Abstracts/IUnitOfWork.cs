using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace hackathon.DataLayer.Abstracts
{
  public interface IUnitOfWork
  {
    void BeginTransaction();
    int SaveChanges();
    Task<int> SaveChangesAsync();
    void Commit();
    void Rollback();
  }
}
