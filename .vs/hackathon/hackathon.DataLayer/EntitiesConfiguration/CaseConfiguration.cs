
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using hackathon.DataLayer.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace hackathon.DataLayer.EntitiesConfiguration
{
  class CaseConfiguration : IEntityTypeConfiguration<Case>
  {
    public void Configure(EntityTypeBuilder<Case> builder)
    {
      builder.ToTable("Cases");

      builder.HasKey(x => x.Id);

      builder.Property(x => x.Title).IsRequired().HasMaxLength(128);
      builder.Property(x => x.SubjectName).IsRequired().HasMaxLength(256);
    }
  }
}
