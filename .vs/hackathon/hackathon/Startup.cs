using hackathon.DataLayer;
using hackathon.DataLayer.Abstracts;
using hackathon.DataLayer.Entities;
using hackathon.Managers;
using hackathon.Managers.Abstracts;
using hackathon.Utility;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hackathon
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddControllers();
      services.AddDbContext<DataContext>(opt => opt.UseNpgsql(Configuration.GetConnectionString("DataConnection")));
      ConfigureIdentity(services);
      ConfigureRepositories(services);
      ConfigureManagers(services);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env, RoleManager<IdentityRole> roleManager)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }

      app.UseHttpsRedirection();

      app.UseRouting();

      app.UseAuthorization();
      SeedRoles(roleManager).Wait();
      app.UseEndpoints(endpoints =>
      {
        endpoints.MapControllers();
      });
    }
     
    private static async Task SeedRoles(RoleManager<IdentityRole> roleManager)
    {
      if (!await roleManager.RoleExistsAsync(UserType.Admin))
      {
        await roleManager.CreateAsync(new IdentityRole(UserType.Admin));
      }
      if (!await roleManager.RoleExistsAsync(UserType.User))
      {
        await roleManager.CreateAsync(new IdentityRole(UserType.User));
      }
    }

    public static void ConfigureIdentity(IServiceCollection services)
    {
      services.AddIdentity<AppUser, IdentityRole>()
          .AddEntityFrameworkStores<DataContext>()
          .AddDefaultTokenProviders();

      //configure identity options
      services.Configure<IdentityOptions>(options =>
      {
        options.Password.RequireDigit = false;
        options.Password.RequireLowercase = false;
        options.Password.RequireUppercase = false;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequiredLength = 6;
      });
    }

    public static void ConfigureRepositories(IServiceCollection services)
    {
      services.AddScoped<IUnitOfWork, UnitOfWork>();
      services.AddScoped<IRepository<Case>, Repository<Case>>();
    }

    public static void ConfigureManagers(IServiceCollection services)
    {
      services.AddScoped<ICaseManager, CaseManager>();
    }

  }
 }
