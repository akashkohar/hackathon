using System;

namespace hackathon.Utility
{
    public struct UserType
    {
        public const string Admin = "admin";
        public const string User = "user";
    }
}
